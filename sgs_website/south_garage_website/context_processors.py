from operator import ne
from cart.models import Cart
from cards.models import Card

def common_context_processor(request):
    user = request.user
    if user.is_active:
        cart = Cart.get_cart(request.user)
        items = cart.cartitem_set.all()
        count = items.count() # получаем общее число товаров в корзине без учета их количества
        first_name_user = user.first_name
        if first_name_user == 'AnonymousUser':
            status_user = True
        else: 
            status_user = False
            
    print('Current User - ', user)

    list = Card.objects.all() # получаем все объекты Card (товары) из базы данных

    def uniq_categories(list):
        unique_categories = []
        for i in range(0, len(list)): # идем по каждому обьекту Card (товары)
            if((list[i].category not in unique_categories) and (list[i].availability)):
                    unique_categories.append(list[i].category) # добавляем в новый список unique_categories категорию обьекта Card (товары), если ее еще не было в новом списке unique_categories и если обьект Card в наличии (доступен)
        return unique_categories # возвращяем список с доступными категориями товаров

    unique_categories = uniq_categories(list) # будем получать из БД уникальные (без повторов) категории товаров

    if user.is_active:
        return {
            'cart': cart,
#            'username': request.COOKIES['username'],
#            'login_status': request.COOKIES['login_status'],
            'items': items,
            'count': count,
            'unique_categories': unique_categories,
            'status_user' : status_user,
        }
    else:
        return {
            'unique_categories': unique_categories,
    }
