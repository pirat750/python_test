import requests
from .models import TeleSettings

def sendTelegram(tg_order_number, tg_name, tg_phone, tg_items, tg_total, tg_delivery_type, tg_address):
    if TeleSettings.objects.get(pk=1):
        settings = TeleSettings.objects.get(pk=1)
        token = str(settings.tg_token)
        chat_id = str(settings.tg_chat)
        text = str(settings.tg_message)
        photo = 'https://kprf72.ru/uploads/news/1c606897bd61.jpeg' #https://avto-russia.ru/moto/honda/photo/honda_giorno_1.jpg'
        api = 'https://api.telegram.org/bot'
        method_Message = api + token + '/sendMessage'
        method_Photo = api + token + '/sendPhoto'

        if text.find('{') and text.find('}') and text.rfind('{') and text.rfind('}'):
            part_1 = text[0:text.find('{')]
            part_2 = text[47:54]
            part_3 = text[62:73]
            part_4 = text[83:99]
            part_5 = text[110:119]
            part_6 = text[129:144]
            part_7 = text[161:179]
            text_slise = part_1 + tg_order_number + part_2 + tg_name + part_3 + tg_phone + part_4 + tg_items + part_5 + tg_total + part_6 + tg_delivery_type + part_7 + tg_address

        else:
            text.slise = text
        try:
            req = requests.post(method_Message, data={
                'chat_id' : chat_id,
                'text' : text_slise
                })

            reqq = requests.post(method_Photo, data={
                'chat_id' : chat_id,
                'photo' : photo
                })

        except:
            pass

        finally:
            if req.status_code != 200:
                print('Ошибка отправки!')
            elif req.status_code == 500:
                print('Ошибка 500')
            else:
                print('Все Ок сообщение отправлено!')

    else:
        pass

def sendTelegramNotification(tg_info):
    if TeleSettings.objects.get(pk=1):
        settings = TeleSettings.objects.get(pk=1)
        token = str(settings.tg_token)
        chat_id = str(settings.tg_chat)
        api = 'https://api.telegram.org/bot'
        method_Message = api + token + '/sendMessage'

        try:
            req = requests.post(method_Message, data={
                'chat_id' : chat_id,
                'text' : tg_info
                })

        except:
            pass

        finally:
            if req.status_code != 200:
                print('Ошибка отправки!')
            elif req.status_code == 500:
                print('Ошибка 500')
            else:
                print('Все Ок сообщение отправлено!')

    else:
        pass