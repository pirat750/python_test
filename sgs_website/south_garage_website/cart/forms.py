from django import forms

from .models import CartItem

class AddQuantityForm(forms.ModelForm):
    class Meta:
        model = CartItem
        fields = ['quantity']
