from django.db import models
from django.contrib.auth.models import User
from cards.models import Card
from django.contrib.auth import get_user_model #It's not recommended to import the User directly as it won't work in projects where the AUTH_USER_MODEL setting has been changed to a different user model. Use the get_user_model method instead:
from orders.models import Payment
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.db.models.signals import post_delete
from django.db.models import Sum
from decimal import Decimal
from django.utils import timezone
from django.utils.timezone import datetime
from django.shortcuts import reverse

User = get_user_model()

class Cart(models.Model):

    STATUS_CART = '1_cart'
    STATUS_WAITING_FOR_PAYMENT = '2_waiting_for_payment'
    STATUS_PAID = '3_paid'
    STATUS_EXPIRED = '4_expired'
    STATUS_CHECK_PAY = '5_check_pay'

    STATUS_CHOICES = [
        (STATUS_CART, 'cart'),
        (STATUS_WAITING_FOR_PAYMENT, 'waiting_for_payment'),
        (STATUS_PAID, 'paid'),
        (STATUS_EXPIRED, 'expired'),
        (STATUS_CHECK_PAY, 'check_pay'),
    ]

    def get_num(date=datetime.now()): # функция для посчета номера заказа (запуск функции в момент создания новой корзины)
        date = date.strftime('%Y%m')[2:6] # получаем текущую дату в нужном нам формате - год, месяц
        if Cart.objects.all().last(): # если есть хоть одна корзина в бд
            last_order = Cart.objects.all().last() # получаем последнюю корзину
            if last_order.order_number[0:4] == date: # проверяем, если год и месяц последнего заказа совпадает с текущими, то
                num = int(last_order.order_number[4::]) + 1 # увеличиваем номер заказа на 1
            else:
                num = 1 # иначе номер заказа начнется с 1
        else:
            return f'{date}1' # если еще нет корзин в базе данных, то присваиваем первому заказу номер 1 с текущей датой
        return(f'{date}{num:04}') # в таком формате функция get_num отдает номер заказа


    order_number = models.CharField(max_length=8, default=get_num, verbose_name='Номер заказа')
    user = models.ForeignKey(User, related_name="cart", on_delete=models.CASCADE, null=True, blank=True, verbose_name='Пользователь')
    status = models.CharField(max_length=200, choices=STATUS_CHOICES, verbose_name='Статус', default=STATUS_CART)
    payment = models.ForeignKey(Payment, on_delete=models.PROTECT, blank=True, null=True)
    total = models.DecimalField(default=0.00, max_digits=25, decimal_places=2, verbose_name='Сумма заказа')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    is_active = models.BooleanField(default=True, verbose_name='Активно')
    is_changed_by_shop = models.BooleanField(default=False, verbose_name='Служебное поле. Корректировка наличия из-за изменения остатка')
    comment = models.TextField(blank=True, null=True)
    delivery_price = models.DecimalField(default=0.00, max_digits=25, decimal_places=2, verbose_name='Стоимость доставки')
    total_with_delivery = models.DecimalField(default=0.00, max_digits=25, decimal_places=2, verbose_name='Итоговая стоимость')
    payment_link = models.CharField(max_length=300, default='null', verbose_name='Платежная ссылка')

    def get_absolute_url(self):
        return reverse("order", kwargs={
           'order_number': self.order_number
        })

    def __unicode__(self):
        return "Id корзины: %s" %(self.id) #
    def __str__(self):
        return f"Корзина: {self.order_number}, её статус {self.status}"
    class Meta:
        verbose_name = 'Корзина'  # verbose это алиас для админки
        verbose_name_plural = 'Корзины'

    @staticmethod
    def get_cart(user: User):
        cart = Cart.objects.filter(user=user, status=Cart.STATUS_CART).first()

        if cart: #and (timezone.now() - cart.created_at.days) > 7: автоматическое удаление корзины после 7 дней бездействия
            pass
            #cart.delete()
            #cart = None

        if not cart:
            cart = Cart.objects.create(user=user, status=Cart.STATUS_CART)
        return cart


    @staticmethod
    def get_order(user: User):
        #print('VOT - ', CartItem.objects.last())
        cart = Cart.objects.filter(user=user, status=Cart.STATUS_WAITING_FOR_PAYMENT).first()

        #dt.timedelta = timezone.now() - cart.created_at
        if cart and (timezone.now() - cart.created_at).days > 1: #автоматическое устаревание неоплаченного заказа после 1 дня бездействия
            print("Заказ просрочен")
            cart = Cart.objects.filter(user=user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_EXPIRED)

        #if not cart:
        #    cart = Cart.objects.create(user=user, status=Cart.STATUS_WAITING_FOR_PAYMENT)
        return cart

    def get_total_price(self):
        total = Decimal(0)
        for item in self.cartitem_set.all(): # с помощью set получаем значения, которые по форейн ки ссылаются на карт
            total += (item.price * item.quantity)
        return total

    @staticmethod
    def get_amount_of_unpaid_orders(owner: User):
        amount = Cart.objects.filter(owner=User, status=Cart.STATUS_WAITING_FOR_PAYMENT).aggregate(Sum('amount'))['amount__sum']
        return amount or Decimal(0)


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, verbose_name='Корзина') # кавычки у Cart были бы нужны, если бы этот класс определен ниже
    card = models.ForeignKey(Card, on_delete=models.PROTECT, verbose_name='Товар')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Количество')
    line_total = models.DecimalField(default=10.00, max_digits=1000, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    price = models.DecimalField(max_digits=20, decimal_places=2, default=0)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    discount = models.DecimalField(max_digits=20, decimal_places=2, default=0)


    def __unicode__(self):
        try:
            return str(self.card.id)
        except:
            return f' {self.card.name} + {self.quantity}'

    def __str__(self):
        return f"{self.cart}, товар {self.card}"

    class Meta:
        verbose_name = 'Товар в корзине'  # verbose это алиас для админки
        verbose_name_plural = 'Товары в корзине'

    @property
    def amount(self): #подсчет суммарной цены для одной позиции
        return self.quantity * ((self.price) - (self.discount))

@receiver(post_save, sender=CartItem) #после сохранения объекта картайтем вызывается функция ниже
def recalculate_cart_amount_after_save(sender, instance, **kwargs):
    cart = instance.cart
    cart.total = cart.get_total_price()
    cart.save()

@receiver(post_delete, sender=CartItem)  # после удаления объекта картайтем вызывается функция ниже
def recalculate_cart_amount_after_delete(sender, instance, **kwargs):
    cart = instance.cart
    cart.total = cart.get_total_price()
    cart.save()

def update_item_amount_in_the_cart(user: User):

    cart = Cart.objects.filter(status=Cart.STATUS_CART, user=user).first()
    #print(cart)
    #print('cart')
    #print('Апдейтик')
    #cart.total = cart.get_total_price()
    items = cart.cartitem_set.all()
    #print('items')
    #print(items)
    for item in items:
        #print('item')
        #print(item)
        #print('СТАРЫЙ item.quantity')
        #print(item.quantity)
        card = Card.objects.get(pk=item.card_id)
        #print('СТАРЫЙ остаток товара card.amount')
        #print(card.amount)
        #print('card.amount - item.quantity')
        #print(card.amount - item.quantity)

        if (card.amount - item.quantity) < 0:
            #print('проверочка запустилась да')
            #print('item.quantity')
            #print(item.quantity)
            item.quantity = card.amount
            item.save()
            #print('item save успешно')
            #print(item.quantity)
            #print('item.quantity=card.amount')
            #print(item.quantity)
            #CartItem.objects.filter(card=card).update(quantity=card.amount)
            #Cart.objects.filter(user=user, status=Cart.STATUS_CART).update(is_changed_by_shop=True)
        #cart.total = cart.get_total_price()
            cart.is_changed_by_shop = True
            cart.total = cart.get_total_price()
            cart.save()
        if item.quantity == 0:
            item.delete()
        cart.save()

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE) # on_delete=models.CASCADE - автоматически удаляет строку из зависимой таблицы, если удаляется связанная строка из главной таблицы
    first_name = models.CharField(max_length=100, blank=True, null=True, verbose_name='Имя')
    last_name = models.CharField(max_length=100, blank=True, null=True, verbose_name='Фамилия')
    surname = models.CharField(max_length=100, blank=False, null=True, verbose_name='Отчество')
    birth_date = models.DateField(blank=True, null=True, verbose_name='Дата рожджения')
    phone = models.CharField(max_length=100, blank=True, null=True, verbose_name='Номер телефона') # number
    address = models.CharField(max_length=100, blank=True, null=True, verbose_name='Адрес доставки')
    zip_code = models.CharField(max_length=100, blank=True, null=True, verbose_name='Почтовый индекс')
    comment = models.CharField(max_length=1000, blank=False, null=True, verbose_name='Комментарий к заказу')
    delivery_type = models.CharField(max_length=100, blank=True, null=True, verbose_name='Почтовый сервис')
    
    def __str__(self):
        return str(self.user)
    
    class Meta:
        verbose_name = 'Данные о заказе'  # verbose это алиас для админки
        verbose_name_plural = 'Данные о заказе'

@receiver(post_save, sender=User) #после создания объекта User вызывается функция ниже
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User) #после сохранения объекта User вызывается функция ниже
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


#@receiver(post_save, sender=User) #после сохранения объекта User вызывается функция ниже
#def calculate_delivery(sender, instance, **kwargs):
#    user = instance.user
#    print(user.profile.)



'''
class Cart(models.Model):
    owner = models.OneToOneField(User, related_name="cart", on_delete=models.CASCADE, null=True, blank=True, verbose_name='Пользователь')
    total = models.DecimalField(default=0.00, max_digits=100, decimal_places=2, verbose_name='Общее количество товаров')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Обновлено')
    is_active = models.BooleanField(default=True, verbose_name='Активно')

    def __unicode__(self):
        return "Id корзины: %s" %(self.id) #
    def __str__(self):
        return f"Корзина пользователя: {self.owner}, в корзине {self.total} товар(ов)"
    class Meta:
        verbose_name = 'Корзина'  # verbose это алиас для админки
        verbose_name_plural = 'Корзины'

class CartItem(models.Model):
    cart = models.ForeignKey('Cart', on_delete=models.CASCADE ,null=True, blank=True, verbose_name='Корзина')
    item = models.ForeignKey(Card, on_delete=models.CASCADE, verbose_name='Товар')
    quantity = models.PositiveIntegerField(default=1, verbose_name='Количество')
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)

    def __unicode__(self):
        return self.item.name #

    class Meta:
        verbose_name = 'Товар в корзине'  # verbose это алиас для админки
        verbose_name_plural = 'Товары в корзине'


#    def make_order(self, user):
        #items = self.cartitem_set.all()
        #items = self.objects.filter(user=user, status=Cart.STATUS_CART)
#        new_status = "STATUS_WAITING_FOR_PAYMENT"
#        self.objects.filter(user=user, status=Cart.STATUS_CART).update(status=new_status)
        #print('items в make_order из моделс')
        #print(items)
        #print(self.status)
        #if items and self.status == Cart.STATUS_CART:
        #if items:
        #    self.status = Cart.STATUS_WAITING_FOR_PAYMENT
        #    print('self.status в моделс ')
        #    print(self.status)
        #    self.save()
        #    self.delete()

        #    self.objects.filter(user=user, status=Cart.STATUS_CART).update(status=STATUS_WAITING_FOR_PAYMENT)
            #cart_item.delete()




'''
