from .models import Cart, CartItem
from cards.models import Card
from datetime import datetime
from django.utils import timezone
from .views import send_cron_message
from telebot.sendmessage_bot import sendTelegramNotification
from datetime import timedelta

def cart_expired_check():
    old_carts = Cart.objects.filter(status=Cart.STATUS_WAITING_FOR_PAYMENT).filter(
        updated_at__lte=timezone.now() - timedelta(days=0)) | Cart.objects.filter(status=Cart.STATUS_CHECK_PAY).filter(
        updated_at__lte=timezone.now() - timedelta(days=0))

    for cart in old_carts:

        # if (timezone.now() - cart.updated_at).days >= 1: #автоматическое устаревание неоплаченного заказа после 1 дня бездействия
        items = cart.cartitem_set.all()

        for item in items:
            card = Card.objects.get(pk=item.card_id)  # получаем товар из товаров

            if card.amount > 0:
                card.amount = card.amount + item.quantity  # вычитаем из остатка на складе количество товара в корзине
                card.save()
                card = None
                item = None

            else:
                card.amount = item.quantity
                card.save()
                card = None
                item = None

            # cart.update(status=Cart.STATUS_EXPIRED)  # Меняем статус корзины
        cart.status = Cart.STATUS_EXPIRED
        cart.save()

    updated_carts = Cart.objects.filter(status=Cart.STATUS_WAITING_FOR_PAYMENT).filter(updated_at__lte = timezone.now() - timedelta(days=0)) | Cart.objects.filter(status=Cart.STATUS_CHECK_PAY).filter(updated_at__lte = timezone.now() - timedelta(days=0))
    info = 'Найдено ' + str(old_carts.count()) + ' просроченных корзин.' + ' После работы крона осталось ' + str(updated_carts.count()) + ' корзин'
    sendTelegramNotification(tg_info=info)
    return 1