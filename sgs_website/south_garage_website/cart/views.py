from django.shortcuts import render, HttpResponseRedirect, redirect
from django.urls import reverse_lazy
from .models import Cart, CartItem, update_item_amount_in_the_cart
from cards.models import Card
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from .forms import AddQuantityForm
from django.utils.decorators import method_decorator
from django.views.generic import DeleteView
from datetime import datetime
from datetime import date
from datetime import timedelta
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from telebot.sendmessage_bot import sendTelegram
from django.utils import timezone
from orders.models import DeliveryType
from django.http import JsonResponse
from yoomoney import Quickpay
from yoomoney import Client
import json
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from orders.models import YooMoneySettings

from django.views.generic import DetailView, ListView


#@login_required(login_url=reverse_lazy('login')) # отключаем проверку на авторизацию, чтобы дать возможность добавлять в корзину товары неавторизованному пользователю
def add_item_to_cart(request, slug):

    if(request.user.is_authenticated == False): # создаем нового псевдоюзера, если не зарегистрированный пользователь добавил товар в корзину
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        now_str = str(now)
        now_data = ''.join(now_str.split()[0].split('-'))
        now_time = ''.join(now_str.split()[1].split(':'))
        inner_username = '{}{}{}'.format('User_', now_data, now_time)
        inner_password = '{}{}'.format(now_time, now_data)
        #inner_first_name = 'AutoAssignedName' # Вариант автоматического присвоения псевдоюзеру такого имени и фамилии
        user = User.objects.create_user(username=inner_username, email=inner_username, password=inner_password, first_name='AnonymousUser', last_name='AnonymousUser',)
        user.save()
        user = authenticate(request, username=inner_username, password=inner_password)
        login(request, user)

    if request.method == 'POST':
        quantity_form = AddQuantityForm(request.POST)
        if quantity_form.is_valid():
            quantity = quantity_form.cleaned_data['quantity']
            if quantity >= 1:
                #print('request.user - ', request.user)
                cart = Cart.get_cart(request.user) #вызываем из моделс функцию по получению корзины
                #card = Card.objects.get(slug=slug)  # пытаемся получить товар
                card = get_object_or_404(Card, slug=slug)
                isNewItem = False
                cart_item, created = CartItem.objects.get_or_create(cart=cart, card=card, quantity=quantity, price=card.new_price)
                #cart = Cart.objects.filter(user=request.user, status=Cart.STATUS_CART)
                #print('cart_item.card - ', cart_item.card) #имя добавляемого товара в корзину
                entrys = Cart.objects.filter(user=request.user, status=Cart.STATUS_CART)
                for e in entrys:
                    prev_card = (e.cartitem_set.filter(card=card))
                    cart_counter = prev_card.count()

                if created:
                    isNewItem = True

                if quantity == 0:
                    cart_item.delete()

                if cart_counter == 1 or cart_counter == 0: #1 возвращается, когда товар уже был в корзине за всё время, а 0, когда добавляется впервые
                    cart = Cart.get_cart(request.user) #получаем корзину
                    item_quantity = cart.cartitem_set.filter(card=card).first() #получаем товар в корзине
                    new_quantity = item_quantity.quantity # присваиваем число товара в корзине

                    if new_quantity == 1 and isNewItem==True: #Если товар только что добавили, то его количество увеличиваться на 1 не будет
                        new_quantity = 0

                    if isNewItem==True: #Если товар только что добавили, то его количество увеличиваться на 1 не будет
                        new_quantity = 0

                    new_quantity += quantity

                    #print('new_quantity', 'card.amount', 'cart_counter')
                    #print(new_quantity, card.amount, cart_counter)

                    if new_quantity > card.amount - cart_counter:
                        new_quantity = card.amount
                        #print('card_delta переопределена')
                    #print('new_quantity')
                    #print(new_quantity)

                    if new_quantity > card.amount - cart_counter: #проверка на превышение остатка
                        new_quantity = card.amount
                        #print('card_delta переопределена')


                    cart_item.quantity = new_quantity
                    cart_item.save()
                    #new_quantity = 0
                    #quantity = 0
                    #isNewItem = False
                else:
                    cart = Cart.get_cart(request.user)
                    item_quantity = cart.cartitem_set.filter(card=card).first()
                    new_quantity = item_quantity.quantity
                    new_quantity += quantity

                    if new_quantity > card.amount - cart_counter: #проверка на превышение остатка
                        new_quantity = card.amount
                        #print('кол-во товаров в корзине превышает остаток')

                    CartItem.objects.filter(card=card).update(quantity=new_quantity)
                    cart_item.delete()
                    new_quantity = 0
                    quantity = 0
            #return redirect('home')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER')) # редиректит на предыдущую страницу

def manual_update_item_amount_in_the_cart(request):
    if request.method == 'GET':
        return redirect('cart')

@login_required(login_url=reverse_lazy('login'))
def cart_view(request):
    #print(request.user)
    cart = Cart.get_cart(request.user)
    items = cart.cartitem_set.all()
    update_item_amount_in_the_cart(request.user)  # вызываем проверку соответствия количества в корзине и в бд
    context = {
        'cart': cart,
        'items': items,
    }
    return render(request, 'cart.html', context)


@login_required(login_url=reverse_lazy('login'))
def view_order(request):
    #print(request.user)
    cart = Cart.get_cart(request.user)
    items = cart.cartitem_set.all()
    update_item_amount_in_the_cart(request.user)  # вызываем проверку соответствия количества в корзине и в бд
    context = {
        'cart': cart,
        'items': items,
    }
    return render(request, 'order.html', context)



#def get_queryset(self):
#    lista = Materiales.objects.all()
#    tipolista = self.kwargs['tipolista']
#    if tipolista=='metales':
        # …
 #   else:
        # …


class OrderDetailView(ListView):
    model = Cart
    template_name = 'view_order.html'
    extra_content = {'title': 'Товар такой-то'}  # таким образом можно передать статическое содержимое
    
    def get_queryset(self):
        print("!!!!!!!!")
        #print(Cart.objects.filter(cart__order_number=self.kwargs['order_number']))
        order_number = self.kwargs['order_number']
        print("order_number - ", order_number)
        return Cart.objects.filter(order_number=self.kwargs['order_number']) #category__slug это обращение к слагу таблицы категори




def plus_cart(request):
    if request.method == 'GET':
        prod_id=request.GET['prod_id']
        cart = Cart.get_cart(request.user)
        c = CartItem.objects.get(cart=cart, card_id=prod_id) #, quantity=quantity, price=card.new_price)
        card = Card.objects.get(pk=c.card_id)

        if c.quantity + 1 <= card.amount:  # проверка на превышение остатка
            c.quantity+=1
            c.save()
        amount=0
        totalamount = cart.get_total_price() #работает
        data={
            'quantity':c.quantity,
            'amount':amount,
            'totalamount':totalamount
        }
        return JsonResponse(data)

def minus_cart(request):
    prod_id = request.GET['prod_id']
    cart = Cart.get_cart(request.user)
    c = CartItem.objects.get(cart=cart, card_id=prod_id)  # , quantity=quantity, price=card.new_price)

    if c.quantity != 1:
        c.quantity -= 1
    c.save()
    amount = 0
    totalamount = cart.get_total_price()  # работает
    data = {
        'quantity': c.quantity,
        'amount': amount,
        'totalamount': totalamount
    }
    return JsonResponse(data)

def remove_cart(request):
    prod_id = request.GET['prod_id']
    cart = Cart.get_cart(request.user)
    c = CartItem.objects.get(cart=cart, card_id=prod_id)  # , quantity=quantity, price=card.new_price)
    c.delete()

    amount = 0
    totalamount = cart.get_total_price()  # работает
    data = {
        'quantity': c.quantity,
        'amount': amount,
        'totalamount': totalamount
    }
    return JsonResponse(data)

@method_decorator(login_required, name='dispatch')
class CartDeleteItem(DeleteView):
    model = CartItem
    template_name = 'cart.html'
    success_url = reverse_lazy('home')

    # Проверка доступа
    def get_queryset(self):
        qs = super().get_queryset()
        qs.filter(cart__user=self.request.user)
        return qs

@login_required(login_url=reverse_lazy('login'))
def make_order(request): # оформление заказа

    cart = Cart.get_cart(request.user)
    items = cart.cartitem_set.all()
    for item in items:


        card = Card.objects.get(pk=item.card_id) #получаем товар из товаров
        # проверка на наличие достаточных товаров на складе

        if (card.amount - item.quantity) >= 0:
            card.amount = card.amount - item.quantity # вычитаем из остатка на складе количество товара в корзине
            card.save()
            #return_order(request)
            Cart.objects.filter(user = request.user, status=Cart.STATUS_CART).update(status=Cart.STATUS_WAITING_FOR_PAYMENT) #Меняем статус корзины

        else:
            update_item_amount_in_the_cart(request.user) #вызываем проверку соответствия количества в корзине и в бд
    return redirect('orders:show_orders')


'''
def yoomoney(sum, label):
    quickpay = Quickpay(
        receiver="410011503475619",
        quickpay_form="shop",
        targets="SOUTH-GARAGE.RU",
        paymentType="SB",
        sum=sum,
        label=label
    )
    payment_url = quickpay.redirected_url
    return payment_url

#@login_required(login_url=reverse_lazy('login'))
def pay_new(request):
    try:
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).last()
        total_with_delivery = order.total + order.delivery_price
        payment_url = yoomoney(sum=total_with_delivery, label=order.total)
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_CHECK_PAY)
        return HttpResponseRedirect(payment_url)

    except:
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_CHECK_PAY).last()
        total_with_delivery = order.total + order.delivery_price
        payment_url = yoomoney(sum=total_with_delivery, label=order.total)
        return HttpResponseRedirect(payment_url)
    finally:
        redirect('orders:show_orders')

def pay(request):
    try:
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).last()
        total_with_delivery = order.total + order.delivery_price
        payment_url = yoomoney(sum=total_with_delivery, label=order.total)
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_CHECK_PAY).update(payment_link=Cart.payment_url)
        return HttpResponseRedirect(payment_url)

    except:
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_CHECK_PAY).last()
        total_with_delivery = order.total + order.delivery_price
        payment_url = yoomoney(sum=total_with_delivery, label=order.total)
        order = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(payment_link=Cart.payment_url)
        return HttpResponseRedirect(payment_url)
    finally:
        redirect('orders:show_orders')
'''

'''
def check_payment(request):
    if YooMoneySettings.objects.get(pk=1):
        settings = YooMoneySettings.objects.get(pk=1)
        token = str(settings.yoo_token)
        client = Client(token)
        history = client.operation_history() #(datetime=today) #(label="a1b2c3d4e5")
        print("List of operations:")
        print("Next page starts with: ", history.next_record)
        for operation in history.operations:
            print()
            print("Operation:",operation.operation_id)
            print("\tStatus     -->", operation.status)
            print("\tDatetime   -->", operation.datetime)
            print("\tTitle      -->", operation.title)
            print("\tPattern id -->", operation.pattern_id)
            print("\tDirection  -->", operation.direction)
            print("\tAmount     -->", operation.amount)
            print("\tLabel      -->", operation.label)
            print("\tType       -->", operation.type)
    return redirect('orders:show_orders')
'''

#STATUS_PAID
'''
@csrf_exempt #disables Django’s default cross-site request forgery (CSRF) protection
@require_POST #blocks non-POST requests
def webhook(request):
    jsondata = request.body
    data = json.loads(jsondata)
    for answer in data['form_response']['answers']: # go through all the answers
      type = answer['type']
      print(f'answer: {answer[type]}') # print value of answers

    return HttpResponse(status=200)
'''

@csrf_exempt #disables Django’s default cross-site request forgery (CSRF) protection
def webhook(request):
    if request.method == 'POST':
        #print("Data received from Webhook is: ", request.body)
        previous_date = str(datetime.today() - timedelta(days=2))
        current_date = str(datetime.today())
        orders = Cart.objects.filter(status=Cart.STATUS_WAITING_FOR_PAYMENT, updated_at__range=[previous_date, current_date])
        if orders is not None and YooMoneySettings.objects.get(pk=1):
            settings = YooMoneySettings.objects.get(pk=1)
            token = str(settings.yoo_token)
            client = Client(token)
            for i in orders:
                history = client.operation_history(label=i.order_number)  # (datetime=today) #(label="a1b2c3d4e5")
                for operation in history.operations:
                    order = Cart.objects.filter(order_number=i.order_number, status=Cart.STATUS_WAITING_FOR_PAYMENT).last()
                    total_with_delivery = order.total + order.delivery_price
                    if operation.status == "success" and round(operation.amount * 1.03) == round(total_with_delivery):
                        Cart.objects.filter(order_number=i.order_number, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_PAID)
                        order = Cart.objects.filter(order_number=i.order_number, status=Cart.STATUS_PAID).last()
                        items = order.cartitem_set.all()
                        #print(items)
                        #print('items')
                        i = 0
                        items_str = ''
                        for item in items:

                            i += 1
                            current_item = Card.objects.get(pk=item.card_id)


                            items_str = items_str + '\n' + str(i) + '. ' + str(current_item) + ' - ' + str(
                                item.quantity) + 'шт.' + '; '
                        user_id = order.user_id
                        user = User.objects.filter(id=user_id).last()
                        fio = user.profile.first_name + ' ' + user.profile.last_name + ' ' + user.profile.surname
                        delivery_id = user.profile.delivery_type
                        user_delivery_type = DeliveryType.objects.get(id=delivery_id)  # получаем данные о типе доставки, который выбрал пользователь в момент оформления заказа
                        sendTelegram(tg_order_number=order.order_number, tg_name=fio,
                                     tg_phone=user.profile.phone, tg_items=items_str,
                                     tg_total=(str(order.total) + ' р. '),
                                     tg_delivery_type=str(user_delivery_type),
                                     tg_address=user.profile.address)
                        return HttpResponse(status=200)
                    else:
                        return HttpResponse(status=200)
        else:
            return HttpResponse(status=200)
        return HttpResponse(status=200)

def return_order(request):
    cart = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).last()
    items = cart.cartitem_set.all()

    for item in items:
        card_id = item.card_id
        card = Card.objects.get(pk=card_id)  # получаем товар из товаров
        if (card.amount + item.quantity) >= 0:
            card.amount = card.amount + item.quantity  # вычитаем из остатка на складе количество товара в корзине
            card.save()

    Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_EXPIRED)  # Меняем статус корзины
    return redirect('orders:show_orders')

'''def pay_for_order(order_number):

    order = Cart.objects.filter(order_number=order_number, status=Cart.STATUS_CHECK_PAY).update(status=Cart.STATUS_PAID)
    cart_paid = Cart.objects.filter(order_number=order_number,status=Cart.STATUS_PAID).last()  # получаем номер заказа, в котором статус корзины - проверить оплату
    order_number_check_pay = cart_paid.order_number
    items = cart_paid.cartitem_set.all()
    items_str = ''
    i = 0
    for item in items:
        i += 1
        current_item = Card.objects.get(pk=item.card_id)
        items_str = items_str + '\n' + str(i) + '. ' + str(current_item) + ' - ' + str(item.quantity) + 'шт.' + '; '
    user_id = cart_paid.user_id
    try:
        user = User.objects.filter(id=user_id).last()
        fio = user.profile.first_name + ' ' + user.profile.last_name + ' ' + user.profile.surname
        delivery_id = user.profile.delivery_type
        user_delivery_type = DeliveryType.objects.get(id=delivery_id) # получаем данные о типе доставки, который выбрал пользователь в момент оформления заказа
        sendTelegram(tg_order_number = order_number_check_pay, tg_name = fio, tg_phone = user.profile.phone, tg_items= items_str, tg_total = (str(cart_paid.total)+' р. '), tg_delivery_type = str(user_delivery_type), tg_address = user.profile.address)
    except:
        sendTelegram(tg_order_number = order_number_check_pay, tg_name = user_id, tg_phone = 'ошибка парсинга телефона', tg_items= items_str, tg_total = (str(cart_paid.total)+' р. '), tg_delivery_type = 'и деливери тайп', tg_address = 'даже адрес не получить')

    return HttpResponse(status=200)'''