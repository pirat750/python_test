# Generated by Django 4.0.5 on 2023-02-19 18:03

import cart.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cards', '0001_initial'),
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_number', models.CharField(default=cart.models.Cart.get_num, max_length=8, verbose_name='Номер заказа')),
                ('status', models.CharField(choices=[('1_cart', 'cart'), ('2_waiting_for_payment', 'waiting_for_payment'), ('3_paid', 'paid'), ('4_expired', 'expired'), ('5_check_pay', 'check_pay')], default='1_cart', max_length=200, verbose_name='Статус')),
                ('total', models.DecimalField(decimal_places=2, default=0.0, max_digits=25, verbose_name='Сумма заказа')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Создано')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Обновлено')),
                ('is_active', models.BooleanField(default=True, verbose_name='Активно')),
                ('is_changed_by_shop', models.BooleanField(default=False, verbose_name='Служебное поле. Корректировка наличия из-за изменения остатка')),
                ('comment', models.TextField(blank=True, null=True)),
                ('delivery_price', models.DecimalField(decimal_places=2, default=0.0, max_digits=25, verbose_name='Стоимость доставки')),
                ('payment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='orders.payment')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='cart', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Корзина',
                'verbose_name_plural': 'Корзины',
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Имя')),
                ('last_name', models.CharField(blank=True, max_length=100, null=True, verbose_name='Фамилия')),
                ('surname', models.CharField(blank=True, max_length=100, null=True, verbose_name='Отчество')),
                ('birth_date', models.DateField(blank=True, null=True, verbose_name='Дата рожджения')),
                ('phone', models.CharField(blank=True, max_length=100, null=True, verbose_name='Номер телефона')),
                ('address', models.CharField(blank=True, max_length=100, null=True, verbose_name='Адрес доставки')),
                ('zip_code', models.CharField(blank=True, max_length=100, null=True, verbose_name='Почтовый индекс')),
                ('delivery_type', models.CharField(blank=True, max_length=100, null=True, verbose_name='Почтовый сервис')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Данные о заказе',
                'verbose_name_plural': 'Данные о заказе',
            },
        ),
        migrations.CreateModel(
            name='CartItem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.PositiveIntegerField(default=1, verbose_name='Количество')),
                ('line_total', models.DecimalField(decimal_places=2, default=10.0, max_digits=1000)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('price', models.DecimalField(decimal_places=2, default=0, max_digits=20)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('discount', models.DecimalField(decimal_places=2, default=0, max_digits=20)),
                ('card', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='cards.card', verbose_name='Товар')),
                ('cart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.cart', verbose_name='Корзина')),
            ],
            options={
                'verbose_name': 'Товар в корзине',
                'verbose_name_plural': 'Товары в корзине',
            },
        ),
    ]
