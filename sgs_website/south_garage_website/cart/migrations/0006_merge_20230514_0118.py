# Generated by Django 4.0.5 on 2023-05-13 22:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0005_alter_cartitem_card_alter_cartitem_cart'),
        ('cart', '0005_cart_payment_link'),
    ]

    operations = [
    ]
