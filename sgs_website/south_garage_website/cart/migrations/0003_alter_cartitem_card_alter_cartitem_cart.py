# Generated by Django 4.0.5 on 2023-05-11 19:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cards', '0002_card_popular'),
        ('cart', '0002_cart_total_with_delivery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartitem',
            name='card',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='carditem', to='cards.card', verbose_name='Товар'),
        ),
        migrations.AlterField(
            model_name='cartitem',
            name='cart',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='cartitem', to='cart.cart', verbose_name='Корзина'),
        ),
    ]
