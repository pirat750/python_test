from django.urls import path
from .views import ItemDetailView # , HomeViewб CategoryView ,getCategory, SortByPopularityView

app_name = 'cards' # This is the namespace so you can reverse urls with core:*

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('<slug>/', ItemDetailView.as_view(), name='product'),
    #path('category/<str:slug>/', CategoryView.as_view(), name='category')
    # path("create/", views.TaskCreateView.as_view(), name='create'),
    # path('', views.index, name="index2"),
    # path("list/", views.TaskListView.as_view(), name='list'),
    # path("create/", views.TaskCreateView.as_view(), name='create'),

]
