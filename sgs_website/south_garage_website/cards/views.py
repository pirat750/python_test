from unicodedata import category
from django.shortcuts import render
from .models import Card
from django.views.generic import ListView, DetailView
#from cards.filter_category import ReceptionCategory

def item_page(request):
    #a = '<h1>Text from views</h1>'
    #text = "Text from views"
    #product_cards = Сard.objects.all()
    return render(request, './item_page.html', {
      #  'a':a,
      #  'text':text,
      #  'product_cards':product_cards
    })

class HomeView(ListView): #ListView создаёт список чего-либо
    model = Card
    template_name = 'index.html'
    #extra_content = {'title': 'Список товаров'}  # таким образом можно передать статическое содержимое
    context_object_name='cards'
    paginate_by = 8 # отображение только 8 карточек товара на одной странице
    allow_empty=False # показываем 404, если нет товаров
#    current_category = ReceptionCategory()
    def get_queryset(self):
        return Card.objects.filter(availability=True, amount__gt=0) #Вывод только товаров, которые есть в наличии # gt - greater then

    def get_context_data(self, *, object_list=None, **kwargs):  # Так можно передать динамическое и статическое содержимое
        context = super().get_context_data(**kwargs)  # тут мы получаем существующий контекст из ListView
        context['title'] = 'Категория - ' + str(context['cards'][0].category)

        #cookie
        if 'login_status' in self.request.COOKIES and 'username' in self.request.COOKIES: # self стоит, так как мы работаем с class based view
            context['username'] = self.request.COOKIES['username']
            context['login_status'] = self.request.COOKIES['login_status']
        #cookie (если в кукисах есть упоминание о том, что вы авторизовались, контекст передается на главную страницу)

        return context

class ItemDetailView(DetailView):
    model = Card
    template_name = 'item_page.html'
    extra_content = {'title': 'Товар такой-то'}  # таким образом можно передать статическое содержимое
    
    #def get_images(self):
    #    card = Card.objects.filter(items=1)
    #    images=card.images_set.all().first()

    #    return


class CategoryView(ListView):
    model = Card
    template_name = 'index.html'
    paginate_by = 4 # отображение только 4 карточек товара на одной странице
    #categories = Category.objects.all()
    context_object_name = 'categories'  #Если хотим в шаблоне использовать не object_list, а for cat in categories
    extra_content = {'title': 'Товар такой-то'} # таким образом можно передать статическое содержимое
    #def get_queryset(self):
    #    return Card.objects.filter(category__slug=self.kwargs['slug'])

    def get_queryset(self):
        return Card.objects.filter(category__slug=self.kwargs['slug'], availability=True) #category__slug это обращение к слагу таблицы категори

class SortByPopularityView(ListView):
    model = Card
    template_name = 'index.html'
    # extra_content = {'title': 'Список товаров'}  # таким образом можно передать статическое содержимое
    context_object_name = 'cards'
    paginate_by = 8  # отображение только 8 карточек товара на одной странице
    allow_empty = False  # показываем 404, если нет товаров

    #    current_category = ReceptionCategory()
    def get_queryset(self):
        return Card.objects.filter(availability=True,
                                   amount__gt=0).order_by('-popular')  # Вывод только товаров, которые есть в наличии # gt - greater then

    def get_context_data(self, *, object_list=None,
                         **kwargs):  # Так можно передать динамическое и статическое содержимое
        context = super().get_context_data(**kwargs)  # тут мы получаем существующий контекст из ListView
        context['title'] = 'Категория - ' + str(context['cards'][0].category)

        # cookie
        if 'login_status' in self.request.COOKIES and 'username' in self.request.COOKIES:  # self стоит, так как мы работаем с class based view
            context['username'] = self.request.COOKIES['username']
            context['login_status'] = self.request.COOKIES['login_status']
        # cookie (если в кукисах есть упоминание о том, что вы авторизовались, контекст передается на главную страницу)

        return context


class SortByMinPriceView(ListView):
    model = Card
    template_name = 'index.html'
    # extra_content = {'title': 'Список товаров'}  # таким образом можно передать статическое содержимое
    context_object_name = 'cards'
    paginate_by = 8  # отображение только 8 карточек товара на одной странице
    allow_empty = False  # показываем 404, если нет товаров

    #    current_category = ReceptionCategory()
    def get_queryset(self):
        return Card.objects.filter(availability=True,
                                   amount__gt=0).order_by(
            '-new_price')  # Вывод только товаров, которые есть в наличии # gt - greater then

    def get_context_data(self, *, object_list=None,
                         **kwargs):  # Так можно передать динамическое и статическое содержимое
        context = super().get_context_data(**kwargs)  # тут мы получаем существующий контекст из ListView
        context['title'] = 'Категория - ' + str(context['cards'][0].category)

        # cookie
        if 'login_status' in self.request.COOKIES and 'username' in self.request.COOKIES:  # self стоит, так как мы работаем с class based view
            context['username'] = self.request.COOKIES['username']
            context['login_status'] = self.request.COOKIES['login_status']
        # cookie (если в кукисах есть упоминание о том, что вы авторизовались, контекст передается на главную страницу)

        return context

class SortByMaxPriceView(ListView):
    model = Card
    template_name = 'index.html'
    # extra_content = {'title': 'Список товаров'}  # таким образом можно передать статическое содержимое
    context_object_name = 'cards'
    paginate_by = 8  # отображение только 8 карточек товара на одной странице
    allow_empty = False  # показываем 404, если нет товаров

    #    current_category = ReceptionCategory()
    def get_queryset(self):
        return Card.objects.filter(availability=True,
                                   amount__gt=0).order_by(
            'new_price')  # Вывод только товаров, которые есть в наличии # gt - greater then

    def get_context_data(self, *, object_list=None,
                         **kwargs):  # Так можно передать динамическое и статическое содержимое
        context = super().get_context_data(**kwargs)  # тут мы получаем существующий контекст из ListView
        context['title'] = 'Категория - ' + str(context['cards'][0].category)

        # cookie
        if 'login_status' in self.request.COOKIES and 'username' in self.request.COOKIES:  # self стоит, так как мы работаем с class based view
            context['username'] = self.request.COOKIES['username']
            context['login_status'] = self.request.COOKIES['login_status']
        # cookie (если в кукисах есть упоминание о том, что вы авторизовались, контекст передается на главную страницу)

        return context


    '''def get_context_data(self, *, object_list=None, **kwargs): #Так можно передать динамическое и статическое содержимое
        context = super().get_context_data(**kwargs) #тут мы получаем существующий контекст из ListView
        context['menu'] = menu
        context['title'] = 'Главная страница'
        return context
    '''

'''def getCategory(request, category_id):
    categories = Category.objects.filter(category_id=category_id)
'''

# Create your views here.