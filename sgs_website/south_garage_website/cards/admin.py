from django.contrib import admin
from .models import Card, Category, Images

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)} # Автоматический перевод поля card_name с русского на английский

class ImagesInline(admin.TabularInline):
    fk_name = 'items'
    model = Images

class ImagesAdmin(admin.ModelAdmin):
    list_display = ('name', 'items')
    list_display_links = ('name', 'items')
    search_fields = ('name', 'items')

admin.site.register(Card, PostAdmin)
admin.site.register(Category)
admin.site.register(Images, ImagesAdmin)

class CardAdmin(admin.ModelAdmin):
    inlines = [ImagesInline,]
