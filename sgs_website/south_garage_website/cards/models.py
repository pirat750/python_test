from django.db import models
from django.shortcuts import reverse
from sorl.thumbnail import ImageField

# Create your models here.

class Card(models.Model):
    #user
    card_id = models.CharField(max_length=100, verbose_name='Артикул', unique=True)
    name = models.CharField(max_length=200, verbose_name='Наименование')
    description = models.CharField(max_length=200, verbose_name='Описание товара')
    old_price = models.PositiveIntegerField(verbose_name='Старая цена')
    new_price = models.PositiveIntegerField(verbose_name='Новая цена')
    amount = models.PositiveIntegerField(default=0, verbose_name='Количество')
    volume = models.PositiveIntegerField(default=1, verbose_name='Объем') # Для каждого товара задем объем, что бы примерно оценить стоимость доставки
    availability = models.BooleanField(default=False, verbose_name='В наличии')
    created = models.DateTimeField(auto_now=True, verbose_name='Дата добавления товара')
    popular = models.PositiveIntegerField(default=1, verbose_name='Популярность (от 1 до 3 (оч популярный)')
    slug = models.SlugField(max_length=255, verbose_name='Url', unique=True)
    category = models.ForeignKey('Category', on_delete=models.PROTECT, null=True, verbose_name='Категория', default=1) # В скобках может быть ссылка (Сategory) на модель или строка с именем модели ('Category'). Ссылку можно использовать, если первичная модель (category) объявлена раньше

    ''' класс статус родитель поля ордер статус
        null - для одобрения пустоты в бд
        blank - для одобрения пустоты в полях админки джанго
    '''

    def __str__(self):
        return self.name # вывод не имени объекта, а имени товара

    def get_absolute_url(self):
        return reverse("cards:product", kwargs={
           'slug': self.slug
        })

    class Meta:
        verbose_name = 'Товар'  # verbose это алиас для админки
        verbose_name_plural = 'Товары'

class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True) #db_index индексирует поле для более быстрого доступа
    slug = models.SlugField(max_length=256, verbose_name='Url', unique=True)

    def __str__(self):
        return self.title

 #   def get_absolute_url(self):
 #       return reverse('category', kwargs={'slug': self.slug})

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

class Images(models.Model):
    name = models.CharField(max_length=100, db_index=True, verbose_name='Название') #db_index индексирует поле для более быстрого доступа
    img = ImageField(upload_to='itemimg/', verbose_name='Фотография')
    items = models.ForeignKey(Card, on_delete=models.CASCADE, related_name='images', verbose_name='Для товара')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'
