from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from orders.forms import Login_form, Register_form
from cart.models import Cart, CartItem, update_item_amount_in_the_cart
from django.shortcuts import redirect
from django.contrib import messages

#def first_page(request): Переписано на классмоделвью
#    product_cards = Card.objects.all()
#    return render(request, './index.html', {
#        'product_cards':product_cards
#    })

def contact(request):
    if request.method == 'GET':
        return render(request, './contact.html')

def about_us(request):
    if request.method == 'GET':
        return render(request, './about-us.html')

def signin(request):
    user = request.user

    if request.method == 'GET':
        form_login = Login_form()
        form_register = Register_form()
        return render(request, './sign-in.html', {'form_login': form_login, 'form_register': form_register})

    if request.method == 'POST':
        user_new = user # Запоминаем псевдоюзера до авторизации и был ли он (иначе запишется AnonymousUser)
        inner_username = request.POST['username']  # В эту переменную записывается то, что мы вводим в поле name
        inner_password = request.POST['password']
        try:
            user = authenticate(request, username=inner_username, password=inner_password) # проверяем, есть ли такой пользователь в базе данных
            context = {
            }
        except Exception:
            return render(request, 'common-error.html')

        if user is not None: # Если авторизация успешная
            user_old = user # Берем текущего юзера
            cart_id = Cart.objects.filter(user=user_old).first().id # Получаем id корзины текущего юзера
            is_avaible = CartItem.objects.filter(cart_id=cart_id).first() # Смотрим, есть ли товары в корзине текущего юзера
            if(user_new.is_authenticated == True and is_avaible is None): # Если товаров у текущего юзера нет, то переносим товары из корзины псевдоюзера
                Cart.objects.filter(user=user_old).delete()
                Cart.objects.filter(user=user_new).update(user=user_old)
            is_redirect = False
            if(user_new.is_authenticated): # если мы пытаемся авторизоваться, уже являясь псевдоюзером, то выходим из акк псевдоюзера и входим в наш акк
                logout(request)
                is_redirect = True
            login(request, user)
            update_item_amount_in_the_cart(request.user)  # вызываем проверку соответствия количества в корзине и в бд
            # Redirect to a success page.
            response = render(request, 'index.html', context)
            response.set_cookie('username', inner_username)
            response.set_cookie('login_status', True)
            if (is_redirect):
                return render(request, './checkout.html') # отрисовка страницы подтверждения заказа
            else:
                return redirect('home')
        else:
            # Return an 'invalid login' error message.

            return render(request, 'wrong-credentials.html')



def logout_view(request):
    '''
    response = HttpResponse('Выход из учетной записи')
    response.delete_cookie('username')
    response.set_cookie('login_status', False)
    '''
    try:
        logout(request)
        return redirect('home')
    except Exception:
        return render(request, 'common-error.html')


def signup(request):

    if request.method == 'GET':
        form_login = Login_form()
        form_register = Register_form()
        return render(request, './sign-in.html', {'form_login': form_login, 'form_register': form_register})

    if request.method == 'POST':
        user = request.user
        form_register = Register_form(request.POST)
        if form_register.is_valid(): # сперва проверяем, верно ли заполнено поле с капчей
            if (user.is_authenticated): # Если при регистрации уже создан псевдоюзер, происходит обновление профиля псевдоюзера
                user.set_password(request.POST['password'])
                user.username = request.POST['username']
                user.first_name = request.POST['first_name']
                user.last_name = request.POST['last_name']
                user.email = request.POST['username']
                user.profile.first_name = request.POST['first_name'] # записываем в профайл юзера его имя, введенное при регистрации
                user.profile.last_name = request.POST['last_name'] # записываем в профайл юзера его фамилию, введенную при регистрации
                        
                try: # Если Username юзера уникален и все ок, то переходим на страницу авторизации
                    user.save()
                    form_login = Login_form()
                    form_register = Register_form()
                    return render(request, './sign-in.html', {'form_login': form_login, 'form_register': form_register})
                except: # Иначе начинаем все по новой
                    form_login = Login_form()
                    form_register = Register_form()
                    return render(request, './sign-up.html', {'form_login': form_login, 'form_register': form_register})
            else:
                inner_username = request.POST['username']  # В эту переменную записывается то, что мы вводим в поле name
                inner_password = request.POST['password']
                inner_first_name = request.POST['first_name']
                inner_last_name = request.POST['last_name']
                set_of_usernames = User.objects.filter(username=inner_username).first() # смотрим, есть ли еще такие логины
                if set_of_usernames == None: # если нет, регаем нового юзера
                    try:
                        user = User.objects.create_user(username=inner_username, email=inner_username, password=inner_password, first_name=inner_first_name, last_name=inner_last_name,)
                        user.profile.first_name = inner_first_name # записываем в профайл юзера его имя, введенное при регистрации
                        user.profile.last_name = inner_last_name # записываем в профайл юзера его фамилию, введенную при регистрации
                        # user_second = UserRegister.objects.create_user(username=inner_username, address=inner_last_addr,)
                        user.save()
                    except Exception:
                        return render(request, 'common-error.html')
                    form_login = Login_form()
                    form_register = Register_form()
                    login(request, user)
                    Cart.get_cart(user) # создаем корзину новому пользователю
                    return redirect('home')
                    #return render(request, './sign-in.html', {'form_login': form_login, 'form_register': form_register}) # переход на форму входа в систему после регистрации нового пользователя

                else: # если есть, рендерим страницу с ошибкой
                    return render(request, 'common-error.html')
        else:
            messages.error(request, "Вы неправильно ввели проверочный код")
            form_login = Login_form()
            return render(request, './sign-in.html', {'form_login': form_login, 'form_register': form_register})