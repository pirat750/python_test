from django.contrib import admin
from django.urls import include, path
from cards.views import HomeView, CategoryView, SortByPopularityView, SortByMaxPriceView, SortByMinPriceView
#from cart.views import view
from cart.views import *
from .views import *
from django.conf.urls import include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth import views as auth_views

app_name = 'core' # This is the namespace so you can reverse urls with core:*

urlpatterns = [
    path('orders/', include('orders.urls', namespace="orders")),


    path('order/<str:order_number>', OrderDetailView.as_view(), name='order'),

    #listarmateriales/<str:tipolista>

    path('admin/', admin.site.urls),
    path('', HomeView.as_view(), name='home'),
    path('popular/', SortByPopularityView.as_view(), name='popular'),
    path('group-by-max-price/', SortByMaxPriceView.as_view(), name='groupbymaxprice'),
    path('group-by-min-price/', SortByMinPriceView.as_view(), name='groupbyminprice'),

    path('product/', include('cards.urls', namespace="cards")),
    path('category/<slug:slug>/', CategoryView.as_view(), name='category'),
    path('contacts', contact, name='contacts'),

    path('cart/', cart_view, name='cart'),
    path('view_order/', view_order, name='view_order'),

    path('add-item-to-cart/<slug:slug>/', add_item_to_cart, name='add_item_to_cart'),
    path('cart_delete_item/<int:pk>/', CartDeleteItem.as_view(), name='cart_delete_item'),
    path('pluscart/', plus_cart),
    path('minuscart/', minus_cart),
    path('removecart/', remove_cart),
    path('update_cart_amount/', manual_update_item_amount_in_the_cart, name='update_cart_amount'),
    path('make_order/', make_order, name='make_order'),
    path('return_order/', return_order, name='return_order'),
    #path('payment/', payment, name='payment'),
    #path('make-a-payment/', pay_for_order, name='make-a-payment'),
    #path('pay/', pay, name='pay'),
    #path('check_payment/', check_payment, name='check_payment'),
    #path('webhook/', webhook),
    path('payments-check/', webhook),
    #path('success/', pay_for_order, name='pay_for_order'),

    # auth
    path('logout/', logout_view, name='logout'),
    path('aboutus/', about_us, name='aboutus'),
    path('sign-in/', signin, name='login'),
    path('sign-up/', signup, name='signup'),
    path('password_reset/',auth_views.PasswordResetView.as_view(),name='password_reset'),
    path('password_reset/done/',auth_views.PasswordResetDoneView.as_view(),name='password_reset_done'),
    path('reset/<uidb64>/<token>/',auth_views.PasswordResetConfirmView.as_view(),name='password_reset_confirm'),
    path('reset/done/',auth_views.PasswordResetCompleteView.as_view(),name='password_reset_complete'),
    path('captcha/', include('captcha.urls')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

'''метод as.view() возвращает функцию, которая может быть вызвана, когда приходит запрос на URL, соответствующий связанному шаблону'''

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns() + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

