from django.contrib import admin
from .models import Order, DeliveryType, YooMoneySettings

# Register your models here.

admin.site.register(Order)
admin.site.register(DeliveryType)
admin.site.register(YooMoneySettings)