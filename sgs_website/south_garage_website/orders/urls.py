from django.urls import path
from . import views
from .views import ItemDetailView

app_name = "orders"

urlpatterns = [
    path('make-order/', views.make_order, name='make-order'),
    #path('success/', pay_for_order, name='pay_for_order'),
    path('show-paid-orders/', views.show_paid_orders, name='show_paid_orders'),
    path('show-expired-orders/', views.show_expired_orders, name='show_expired_orders'),
    path('show-orders/', views.show_orders, name='show_orders'),
    #path('paymentsss/', views.make_payment, name='make_payment'),
    #path('info/', views.get_info, name='get_info'),
    path('<order_id>/', ItemDetailView.as_view(), name='view_order'),
    #path('sign-up', views.create_user),
    #path('sign-in', views.sign_in),
    #path('logout', views.logout),
    # path('admin/', admin.site.urls),
    # path("create/", views.TaskCreateView.as_view(), name='create'),
    # path('', views.index, name="index2"),
    # path("list/", views.TaskListView.as_view(), name='list'),
    # path("create/", views.TaskCreateView.as_view(), name='create'),
]


