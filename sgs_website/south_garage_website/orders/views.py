from django.shortcuts import render, redirect
from telebot.sendmessage_bot import sendTelegram
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from cart.models import Cart
from orders.models import DeliveryType
from south_garage_website.views import signin
from orders.forms import Order_form
#from .payment import Authorize
from yoomoney import Client
from .models import Order
from django.views.generic import DetailView
from yoomoney import Quickpay
#from yoomoney import Client
from orders.models import YooMoneySettings

from cart.models import update_item_amount_in_the_cart



def yoomoney(sum, label):
    quickpay = Quickpay(
        receiver="410011503475619",
        quickpay_form="shop",
        targets="SOUTH-GARAGE.RU",
        paymentType="SB",
        sum=sum,
        label=label
    )
    payment_url = quickpay.redirected_url
    return payment_url

class ItemDetailView(DetailView):
    model = Order
    template_name = 'orders.html'
    extra_content = {'title': 'Товар такой-то'}  # таким образом можно передать статическое содержимое
    context_object_name = 'view_order'

def send_message(request):
    name = 'Foma Kinyaev'
    phone = '+7-921-002-54-67'
    sendTelegram(tg_name = name, tg_phone = phone)
    return render(request, './checkout.html')

@login_required(login_url=reverse_lazy('login'))
def make_order(request):
    user = request.user
    if(user.first_name == 'AnonymousUser' and user.last_name == 'AnonymousUser'):
        return signin(request)
    elif request.method == 'GET':
        #print('GET')
        form = Order_form()
        #print(form)
        return render(request, './checkout.html', {
            'form': form
            })
    elif request.method == 'POST':
        #update_item_amount_in_the_cart(request.user)  # вызываем проверку соответствия количества в корзине и в бд
        form = Order_form()
        user.profile.first_name = request.POST['first_name'] # записываем в профайл юзера
        user.profile.last_name = request.POST['last_name']     
        user.profile.address = request.POST['address'] 
        user.profile.phone = request.POST['phone'] 
        user.profile.surname = request.POST['surname'] 
        user.profile.zip_code = request.POST['zip_code'] 
        user.profile.comment = request.POST['comment']
        user.profile.delivery_type = request.POST['delivery_type'] 
        user.save()
       
        cart = Cart.objects.filter(user=request.user, status=Cart.STATUS_CART).last() # получаем текущую корзину пользователя
        items_cart = cart.cartitem_set.all() # получаем набор товаров из корзины
        max_volume = 0
        for item in items_cart: # определяем максимальный обьемный коэффициент товара в текущей корзине для расчета стоимости доставки
            if(item.card.volume > max_volume): # если текущий товар имеет больший обьемный коэффициент, чем предыдущие, записываем этот коэффициент в переменную max_volume
                max_volume = item.card.volume
        user_delivery_type = DeliveryType.objects.filter(id=user.profile.delivery_type).last() # получаем данные о типе доставки, который выбрал пользователь в момент оформления заказа
        if(int(user_delivery_type.base_price) == 0):
            delivery_price = 0
        else:
            delivery_price = int(user_delivery_type.base_price) + (int(max_volume)*100) # производим рассчет стоимости доставки на основе базовой цены доставки выбранного способа доставки и максимального обьемного коэффициента в корзине пользователя
        total_with_delivery = cart.total + delivery_price
        Cart.objects.filter(user=request.user, status=Cart.STATUS_CART).update(delivery_price=delivery_price) # записываем вычисленную стоимость доставки в таблицу с корзинами
        Cart.objects.filter(user=request.user, status=Cart.STATUS_CART).update(total_with_delivery=total_with_delivery) # записываем вычисленную стоимость доставки в таблицу с корзинами
        cart = Cart.objects.filter(user=request.user, status=Cart.STATUS_CART).last() # получаем текущую корзину пользователя
        total_with_delivery = cart.total_with_delivery
        payment_url = yoomoney(sum=cart.total_with_delivery, label=cart.order_number)
        #Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).update(status=Cart.STATUS_CHECK_PAY)
        Cart.objects.filter(user=request.user, order_number=cart.order_number).update(payment_link=payment_url)

        response = redirect('make_order') #render(request, 'index.html') #{% url 'make_order' %}"
        return response


@login_required(login_url=reverse_lazy('login'))
def show_orders(request):

    #order_number_waiting_for_payment = Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT).last()
    
    orders = Cart.objects.filter(user=request.user, status=Cart.STATUS_PAID) | Cart.objects.filter(user=request.user, status=Cart.STATUS_WAITING_FOR_PAYMENT) | Cart.objects.filter(user=request.user, status=Cart.STATUS_CHECK_PAY) | Cart.objects.filter(user=request.user, status=Cart.STATUS_EXPIRED)
    #orders = orders.order_by('-order_number') now in dev

    if orders.exists(): # метод .exists() возвращает True, если запрос query не пустой
        is_stock = True
    else:
        is_stock = False
    #print('orders', orders)

    try:
        context = {
            'orders': orders,
            'is_stock': is_stock,
        }
    except:
        is_stock = False
        context = {
            'orders': orders,
        }
 
    return render(request, 'orders.html', context)




@login_required(login_url=reverse_lazy('login'))
def show_paid_orders(request):
    order_number_pay = Cart.objects.filter(user=request.user, status=Cart.STATUS_PAID).last() # получаем номер заказа, в котором статус корзины - оплачено
    try:
        items_order_number_pay=order_number_pay.cartitem_set.all()  # получаем содержимое дочерней модели Cart - Cartitem (список товаров в корзине)
        context = {
            'order_number_pay': order_number_pay,
            'items_order_number_pay': items_order_number_pay,
        }
    except:
        context = {
            'order_number_pay': order_number_pay,
        }

    return render(request, 'paid-orders.html', context)

@login_required(login_url=reverse_lazy('login'))
def show_expired_orders(request):
    order_number_expired = Cart.objects.filter(user=request.user, status=Cart.STATUS_EXPIRED).last() # получаем номер заказа, в котором статус корзины - отменена
    #order_number_expired = Cart.objects.filter(user=request.user, status=Cart.STATUS_EXPIRED).last() 
        
    try:
        items_order_number_expired=order_number_expired.cartitem_set.all() # получаем содержимое дочерней модели Cart - Cartitem (список товаров в корзине)
        context = {
            'order_number_expired': order_number_expired,
            'items_order_number_expired': items_order_number_expired, # список товаров в корзине
        }
    except:
        context = {
            'order_number_expired': order_number_expired,
        }

    return render(request, 'expired-orders.html', context)


'''
def get_info(request):
    token = ""
    client = Client(token)
    user = client.account_info()
    print("Account number:", user.account)
    print("Account balance:", user.balance)
    print("Account currency code in ISO 4217 format:", user.currency)
    print("Account status:", user.account_status)
    print("Account type:", user.account_type)
    print("Extended balance information:")
    for pair in vars(user.balance_details):
        print("\t-->", pair, ":", vars(user.balance_details).get(pair))
    print("Information about linked bank cards:")
    cards = user.cards_linked
    if len(cards) != 0:
        for card in cards:
            print(card.pan_fragment, " - ", card.type)
    else:
        print("No card is linked to the account")
    return render(request, './index.html')
'''