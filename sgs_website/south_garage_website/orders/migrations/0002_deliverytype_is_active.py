# Generated by Django 4.0.5 on 2023-03-19 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliverytype',
            name='is_active',
            field=models.BooleanField(default=True, verbose_name='Активно'),
        ),
    ]
