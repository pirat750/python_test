from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import reverse
    # Create your models here.

class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True, null=True)

class Order(models.Model):
    order_id = models.CharField(max_length=100, verbose_name='Номер заказа')
    total_price = models.CharField(max_length=100, verbose_name='Итоговая цена с доставкой')
    payment_method = models.CharField(max_length=100, verbose_name='Способ доставки')
    is_paid = models.BooleanField(default=False, verbose_name='Факт оплаты')
    paid_at = models.DateTimeField(verbose_name='Дата оплаты')
    delivered_at = models.DateTimeField(verbose_name='Дата доставки')
    created_at = models.DateTimeField(auto_now=True, verbose_name='Дата создания заказа')
    track_number = models.CharField(max_length=100, verbose_name='Номер для отслеживания')

    #order_status = models.ForeignKey(StatusCRM, on_delete=models.PROTECT, null= True, blank=True)

    ''' класс статус родитель поля ордер статус
        null - для одобрения пустоты в бд
        blank - для одобрения пустоты в полях админки джанго
    '''

    def __str__(self):
        return self.order_id # вывод не имени объекта, а имени заказа
    
    def get_absolute_url(self):
        return reverse("orders:view_order", kwargs={
           'order_id': self.order_id
        })
    
    class Meta:
        verbose_name = "Заказ"  # verbose это алиас для админки
        verbose_name_plural = "Заказы"

class DeliveryType(models.Model):
    delivery = models.CharField(blank=True, null=True, max_length=100, verbose_name='Служба доставки')
    base_price = models.CharField(blank=True, null=True, max_length=100, verbose_name='Базовая стоимость доставки')
    is_active = models.BooleanField(default=True, verbose_name='Активно')

    def __str__(self):
        return self.delivery # вывод не имени объекта, а имени заказа

    class Meta:
        verbose_name = "Служба доставки"  # verbose это алиас для админки
        verbose_name_plural = "Службы доставки"


class YooMoneySettings(models.Model):
    yoo_token = models.CharField(max_length=400, verbose_name='Токен из ЛК Юмани')
    redirect_url = models.CharField(max_length=200, verbose_name='Ссылка на редирект')

    def __str__(self):
        return self.yoo_token

    class Meta:
        verbose_name = 'Настройка Юмани'
        verbose_name_plural = 'Настройки Юмани'