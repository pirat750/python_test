from django import forms
from .models import DeliveryType
from captcha.fields import CaptchaField


class Login_form(forms.Form):
    username = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))

class Register_form(forms.Form):
    username = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    last_name = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(max_length=30, required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    captcha = CaptchaField()

class Order_form(forms.Form):
    delivery_type = forms.ModelChoiceField(queryset=DeliveryType.objects.filter(is_active=True).all(), empty_label="Выбрать") # Параметр to_field_name="delivery" обозначет, по какому столбцу таблицы DelyveryType будут отображаться значения в форме
    address = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Москва, ул. Академика Королева, 12'}))
    phone = forms.CharField(max_length=16, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': '+7 (123) 456-78-90'}))
    zip_code = forms.CharField(max_length=10, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': '127427'}))
    first_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Иван'}))
    last_name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Иванов'}))
    surname = forms.CharField(max_length=100, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Иванович'}))
    comment = forms.CharField(max_length=1000, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Любая дополнительная информация'}))


